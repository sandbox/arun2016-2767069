<?php

/*** For Creating Modal Demo form */  
 function modal_form_demo($ajax, $id) {  
   if ($ajax) {  
     ctools_include('ajax');  
     ctools_include('modal');  
     $form_state = array(  
       'ajax' => TRUE,  
       'title' => t('Modal Form Demo'),  
       'disputeId' => $id,  
     );  
     $output = ctools_modal_form_wrapper('example_form', $form_state);  
     // If the form has been submitted, there may be additional instructions  
     // such as dismissing the modal popup.  
     if (!empty($form_state['ajax_commands'])) {  
       $output = $form_state['ajax_commands'];  
     }  
     // Return the ajax instructions to the browser via ajax_render().  
     print ajax_render($output);  
     drupal_exit();  
   } else {  
     return drupal_get_form('example_form');  
   }  
 }  
 function example_form($form, $form_state) {  
   $form = array();  
   /* Get all reasons from texonomay */  
   $taxonomyTree = taxonomy_get_tree(1);  
   $taxonomyOptions = array();  
   foreach ($taxonomyTree as $taxonomy) {  
     $taxonomyOptions[$taxonomy->tid] = $taxonomy->name;  
   }  
   $form['select_terms'] = array(  
     '#type' => 'select',  
     '#title' => t('Select Terms'),  
     '#options' => $taxonomyOptions,  
    // '#default_value' => $category['selected'],  
   );  
   $form['disputes_id'] = array(  
     '#type' => 'hidden',  
     '#default_value' => $form_state['disputeId'],  
   );  
   $form['auction_id'] = array(  
     '#type' => 'hidden',  
     '#default_value' => 11,  
   );  
   $form['bidder_id'] = array(  
     '#type' => 'hidden',  
     '#default_value' => 11,  
   );  
   $form['submit'] = array(  
     '#type' => 'submit',  
     '#value' => t('Submit'),  
     '#attributes' => array('onclick' => 'if(!confirm("Are you sure you want to submit this form?")){return false;}'),  
   );  
   return $form;  
 }  
 /**  
  * Drupal form submit handler.  
  */  
 function example_form_submit(&$form, &$form_state) {  
   global $user;  
   global $base_url;  
   $val = $form_state['values'];  
   $form_state['ajax_commands'][] = ctools_modal_command_dismiss();  
   $form_state['ajax_commands'][] = ctools_ajax_command_reload();  
   drupal_set_message("Successfully submitted. ".$val['select_terms']);  
 }  