<?php

/**
 * Inc file contains the functions
 */

/**
 * Settings form.
 * @param array $form
 * @param array $form_state
 * @return array
 */
function brochure_settings_form($form, &$form_state) {
  // General API settings
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('brochure Set Mail'),
    '#description' => t(''),
  );
  $form['brochure_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email ID'),
    '#default_value' => variable_get('brochure_email', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Download brochure
 * @global type $base_url
 * @param int $event_id
 */
function brochure_download($event_id) {
  global $base_url;

  if (!$event_id) {
    $event_id = 1;
  }
  $where = " AND n.nid = $event_id";


  $rbrochure = db_query("SELECT n.nid AS nid,l.field_brochure_fid AS fid FROM node AS n LEFT JOIN field_data_field_brochure AS l ON n.nid = l.entity_id AND (l.entity_type = 'node' AND l.deleted = '0') WHERE ((n.status = '1') AND n.type IN  ('event') " . $where . ") ORDER BY n.created DESC")->fetchObject();
  $file = file_load($rbrochure->fid);
  $uri = $file->uri;
  $url = file_create_url($uri);
  $filename = $url;

  $fileinfo = pathinfo($filename);
  $sendname = $fileinfo['filename'] . '.' . strtoupper($fileinfo['extension']);

  header('Content-Type: application/pdf');
  header("Content-Disposition: attachment; filename=\"$sendname\"");
  header('Content-Length: ' . filesize($filename));
  readfile($filename);
}

?>
