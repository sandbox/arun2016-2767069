var SITE_KEY = '6LdSIwUTAAAAAE5SKk2gv1_iLOVxMLHHnOttLgZc';
var app = angular.module('contactApp', ['reCAPTCHA']);

app.config(function (reCAPTCHAProvider) {
    // required, please use your own key :)
    reCAPTCHAProvider.setPublicKey(SITE_KEY);
    // optional
    reCAPTCHAProvider.setOptions({
        theme: 'clean',
        tabindex: 2
    });
});


app.controller('ContactController', function ($scope, $http, reCAPTCHA) {

    $scope.success = false;
    $scope.error = false;
    $scope.result = 'hidden'

    $scope.resultMessage;
    $scope.resultDownload;
    $scope.formData; //formData is an object holding the name, email, subject, and message
    $scope.submitButtonDisabled = false;
    $scope.submitted = false; //used so that form errors are shown only after the form has been submitted
    $scope.submit = function (contactform) {
        $scope.submitted = true;
        $scope.submitButtonDisabled = true;
        if (contactform.$valid) {
            $http({
                method: 'POST',
                url: '/brochure/send',
                data: $scope.formData, //param method from jQuery
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}  //set the headers so angular passing info as form data (not request payload)
            }).success(function (data) {
                console.log(data);
                if (data.success) { //success comes from the return json object
                    $scope.submitButtonDisabled = true;
                    $scope.resultMessage = data.message;
                    $scope.resultDownload = data.event_id;
                    $scope.success = true;
                    $scope.result = 'bg-success';
                    $scope.error = false;
                } else {
                    $scope.submitButtonDisabled = false;
                    $scope.resultMessage = data.message;
                    $scope.result = 'bg-danger';
                    $scope.error = true;
                    //reloadCaptcha();
                    Recaptcha.reload();
                }
            });
        } else {
            $scope.submitButtonDisabled = false;
            $scope.resultMessage = 'Failed : Please fill out all the fields.';
            $scope.result = 'bg-danger';
            $scope.error = true;
        }
    }
});


function reloadCaptcha() {
    $('#recaptcha_reload').click();
}

jQuery(document).ready(function ($) {
    $(".brochure-div").hide();
    $(".iipla_brochure").click(function () {
        $(".brochure-div").toggle();
    });

    $(".iipla_reserve").hide();
    $(".reserve_palce").click(function () {
        $(".iipla_reserve").toggle();
    });

});
