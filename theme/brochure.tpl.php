<?php
$nodes = arg(1);
global $user;
$name = $user->name;
$email = $user->mail;
?>
<div ng-app="contactApp" class="brochure-div">
    <div class="panel panel-default">
        <div ng-controller="ContactController" class="panel-body">


            <form ng-submit="submit(contactform)" name="contactform" method="post" action="" class="form-horizontal" role="form" novalidate>
                <div ng-show="success">
                    <p class="brochure-message">{{ resultMessage}}</p>   
                    <p class="brochure-download"><a href="{{ resultDownload}}">Download</a></p>
                </div>
                <div ng-show="error">
                    <p class="brochure-error">{{ resultMessage}}</p>   
                </div>
                <div ng-hide="success"class="brochure-main">

                    <div class="form-group"  ng-class="{ 'has-error': contactform.inputName.$invalid && submitted }">

                        <div class="col-lg-12">
                            <input ng-model="formData.inputName" type="text" class="form-control" id="inputName" name="inputName"  ng-init = "formData.inputName = '<?php print $name; ?>'"  placeholder="Your Name" required>

                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error': contactform.inputEmail.$invalid && submitted }">

                        <div class="col-lg-12">
                            <input ng-model="formData.inputEmail" type="email" class="form-control" id="inputEmail" name="inputEmail" ng-init= "formData.inputEmail = '<?php print $email; ?>'" placeholder="Your Email" required>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error': contactform.inputPhone.$invalid && submitted }">

                        <div class="col-lg-12">
                            <input ng-model="formData.inputPhone" type="text" class="form-control" id="inputPhone" name="inputPhone" placeholder="Your Phone / Mobile" required>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error': contactform.inputCountry.$invalid && submitted }">

                        <div class="col-lg-12">
                            <input ng-model="formData.inputCountry" type="text" class="form-control" id="inputCountry" name="inputCountry" placeholder="Your Country" required>

                            <input ng-model="formData.inputNid" type="hidden" autocomplete="off" ng-init="formData.inputNid = <?php print $nodes; ?>"  id="inputNid" name="inputNid"> 

                        </div>
                    </div>


                    <div class="form-group" ng-class="{ 'has-error': contactform.recaptcha_response_field.$invalid && submitted }">

                        <div class="col-lg-12"> 
                            <div re-captcha ng-model="formData.captcha"></div>

                        </div> 
                    </div>




                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn bouchre-button" ng-disabled="submitButtonDisabled">
                                Download
                            </button>
                        </div>
                    </div>
                </div>  
            </form>
        </div>
    </div>
</div>
